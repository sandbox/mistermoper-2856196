<?php
/**
 * @file
 * Contains \Drupal\restful_webform\ResourceWebform.
 */

namespace Drupal\restful_webform\Plugin\resource;

use Drupal\restful\Http\RequestInterface;
use Drupal\restful\Plugin\resource\ResourceInterface;
use Drupal\restful\Plugin\resource\Resource;
use Drupal\restful\Exception\NotFoundException;
use Drupal\restful\Exception\BadRequestException;
use Drupal\restful\Exception\ForbiddenException;

/**
 * Class Webform.
 *
 * @Resource(
 *   name = "webform:1.0",
 *   resource = "webform",
 *   label = "Webform",
 *   description = "Send webform to drupal.",
 *   authenticationTypes = TRUE,
 *   authenticationOptional = TRUE,
 *   majorVersion = 1,
 *   minorVersion = 0,
 * )
 */
class ResourceWebform extends Resource implements ResourceInterface {

  /**
   * Overrides \RestfulBase::controllersInfo().
   */
  public function controllersInfo() {
    return array(
      '^.*$' => array(
        RequestInterface::METHOD_POST => 'sendWebform',
      ),
    );
  }

  /**
   * Callback for webform resource.
   *
   * Get neccesary data and send webform.
   */
  public function sendWebform() {
    $fields = $this->getRequest()->getParsedBody();
    $webform_id = $this->path;
    $webform_node = node_load($webform_id);

    if (!$webform_node || $webform_node->type != 'webform') {
      throw new NotFoundException(t('Webform "@webform" does not exists.', array('@webform' => $webform_id)));
    }

    if ($this->checkWebformSubmitAccess($webform_node) === FALSE) {
      // User does not have access to submit webform.
      throw new ForbiddenException('You do not have access to submit a webform.');
    }

    $data = $this->processWebformComponents($webform_node, $fields);
    $this->validateWebformRequiredFields($webform_node, $data);
    $this->webformSubmission($webform_id, $data);
  }

  /**
   * Check user webform access.
   *
   * @param object $webform
   *   Webform.
   *
   * @return boolean
   *   User access result.
   */
  public function checkWebformSubmitAccess($webform) {
    $user_is_allowed = FALSE;
    _webform_allowed_roles($webform, $user_is_allowed);
    return $user_is_allowed;
  }

  /**
   * Process webform components to build data neccesary for submission.
   *
   * Builds an array cid => value given an array built of
   * component_name => value.
   *
   * @param array $components
   *   Fields .
   *
   * @return type
   * @throws BadRequestException
   */
  public function processWebformComponents($webform_node, $components) {
    $data = array();
    foreach ($webform_node->webform['components'] as $cid => $info) {
      $value = !empty($components[$info['form_key']]) ? $components[$info['form_key']] : webform_replace_tokens($info['value'], $webform_node);
      $data[$cid] = !empty($value) ? array(check_plain($value)) : NULL;
    }
    return $data;
  }

  /**
   * Submit webform.
   *
   * @param int $webform_id
   *   Webform id.
   * @param array $data
   *   Webform data.
   */
  public function webformSubmission($webform_id, $data) {
    global $user;
    $webform = node_load($webform_id);
    $submission = (object) array(
      'nid' => $webform->nid,
      'uid' => $user->uid,
      'submitted' => REQUEST_TIME,
      'remote_addr' => ip_address(),
      'is_draft' => FALSE,
      'data' => $data,
      'serial' => _restful_webform_service_submission_serial_next_value($webform->nid),
    );
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    webform_submission_insert($webform, $submission);
    webform_submission_send_mail($webform, $submission);
  }

  /**
   * {@inheritdoc}
   */
  protected function publicFields() {
    // @NOTE: only use this if you extend the class for a specific webform!
    return array();
  }

  /**
   * Validate webform required fields.
   *
   * @param object $webform_node
   *   Webform.
   * @param array $data
   *   Data.
   *
   * @throws BadRequestException
   *   Throw error when a field is required.
   */
  protected function validateWebformRequiredFields($webform_node, $data) {
    foreach ($webform_node->webform['components'] as $component) {
      if (empty($data[$component['cid']]) && $component['required']) {
        throw new BadRequestException(
          t('Field "@component" is required!',
            array(
              '@component' => $component['name'],
              '@webform' => $webform_node->title,
            )
          )
        );
      }
    }
  }

}
